package com.fl.accelatest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccelatestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccelatestApplication.class, args);
	}

}
