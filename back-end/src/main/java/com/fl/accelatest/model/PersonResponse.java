package com.fl.accelatest.model;

import com.fl.accelatest.entity.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonResponse {
    private Long id;
    private String firstName;
    private String lastName;
    private List<Address> addresses;
}
