package com.fl.accelatest.controller;

import com.fl.accelatest.model.AddressDto;
import com.fl.accelatest.model.AddressRequest;
import com.fl.accelatest.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/addresses")
public class AddressController {
    @Autowired
    AddressService addressService;

    @PutMapping("/{addressId}")
    public ResponseEntity<AddressDto> updateAddress(@PathVariable(value = "addressId") long addressId,
                                                    @RequestBody AddressRequest addressRequest) {
        AddressDto addressDto = new AddressDto(addressId, addressRequest.getStreet(), addressRequest.getCity(),
                addressRequest.getState(), addressRequest.getPostalCode());
        AddressDto savedAddressDto = addressService.updateAddress(addressDto);

        return new ResponseEntity<>(savedAddressDto, HttpStatus.OK);
    }

    @DeleteMapping("/{addressId}")
    public ResponseEntity<AddressDto> delete(@PathVariable(value = "addressId") long addressId) {
        AddressDto deleteAddress = addressService.deleteAddress(addressId);

        return new ResponseEntity<>(deleteAddress, HttpStatus.OK);
    }
}
