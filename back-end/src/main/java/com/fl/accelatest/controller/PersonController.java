package com.fl.accelatest.controller;

import com.fl.accelatest.model.AddressDto;
import com.fl.accelatest.model.AddressRequest;
import com.fl.accelatest.model.PersonDto;
import com.fl.accelatest.model.PersonRequest;
import com.fl.accelatest.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/persons")
public class PersonController {
    @Autowired
    PersonService personService;

    @GetMapping()
    public ResponseEntity<List<PersonDto>> getAllPersons() {
        List<PersonDto> allPersons = personService.allSavedPersons();

        return new ResponseEntity<>(allPersons, HttpStatus.OK);
    }

    @PostMapping("/{personId}")
    public ResponseEntity<PersonDto> createPerson(@PathVariable(value = "personId") long personId, @RequestBody PersonRequest personRequest) {
        PersonDto personDto = new PersonDto(personId, personRequest.getFirstName(), personRequest.getLastName(), new ArrayList<>());
        PersonDto savedPersonDto = personService.savePerson(personDto);

        return new ResponseEntity<>(savedPersonDto, HttpStatus.OK);
    }

    @PutMapping("/{personId}")
    public ResponseEntity<PersonDto> updatePerson(@PathVariable(value = "personId") long personId, @RequestBody PersonRequest personRequest) {
        PersonDto personDto = new PersonDto(personId, personRequest.getFirstName(), personRequest.getLastName(), new ArrayList<>());
        PersonDto savedPersonDto = personService.updatePerson(personDto);

        return new ResponseEntity<>(savedPersonDto, HttpStatus.OK);
    }

    @DeleteMapping("/{personId}")
    public ResponseEntity<PersonDto> delete(@PathVariable(value = "personId") long personId) {
        PersonDto deletedPerson = personService.deletePerson(personId);

        return new ResponseEntity<>(deletedPerson, HttpStatus.OK);
    }

    @PutMapping("/{personId}/addresses/{addressId}")
    public ResponseEntity<PersonDto> addAddressToPerson(@PathVariable(value = "personId") long personId,
                                                  @PathVariable(value = "addressId") long addressId,
                                                  @RequestBody AddressRequest addressRequest) {
        AddressDto addressDto = new AddressDto(addressId, addressRequest.getStreet(), addressRequest.getCity(),
                addressRequest.getState(), addressRequest.getPostalCode());
        PersonDto updatedPerson = personService.addAddress(personId, addressDto);

        if (updatedPerson == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        return new ResponseEntity<>(updatedPerson, HttpStatus.OK);
    }
}
