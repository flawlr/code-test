package com.fl.accelatest.service;

import com.fl.accelatest.model.AddressDto;
import com.fl.accelatest.model.PersonDto;
import com.fl.accelatest.entity.Address;
import com.fl.accelatest.entity.Person;
import com.fl.accelatest.mapper.AddressMapper;
import com.fl.accelatest.mapper.PersonMapper;
import com.fl.accelatest.respository.AddressRepository;
import com.fl.accelatest.respository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PersonService {
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private AddressMapper addressMapper;

    public PersonDto savePerson(PersonDto personDto) {
        Person person = personMapper.dtoToPerson(personDto);
        Person savedPerson = personRepository.save(person);
        return personMapper.personToDto(savedPerson);
    }

    public PersonDto updatePerson(PersonDto personDto) {
        Optional<Person> existingPersonOptional = personRepository.findById(personDto.getId());
        if (existingPersonOptional.isPresent()) {
            Person existingPerson = existingPersonOptional.get();
            if (personDto.getFirstName() != null) {
                existingPerson.setFirstName(personDto.getFirstName());
            }
            if (personDto.getLastName() != null) {
                existingPerson.setLastName(personDto.getLastName());
            }
            Person updatedPerson = personRepository.save(existingPerson);
            return personMapper.personToDto(updatedPerson);
        }
        Person newPerson = personRepository.save(personMapper.dtoToPerson(personDto));
        return personMapper.personToDto(newPerson);
    }

    @Transactional
    public PersonDto deletePerson(Long id) {
        Optional<Person> person = personRepository.findById(id);
        if (person.isPresent()) {
            personRepository.deleteById(id);
            return personMapper.personToDto(person.get());
        }
        return null;
    }

    @Transactional
    public PersonDto addAddress(Long personId, AddressDto addressDto) {
        Optional<Person> optionalPerson = personRepository.findById(personId);
        if (optionalPerson.isPresent()) {
            Person person = optionalPerson.get();
            Address address = addressRepository.save(addressMapper.dtoToAddress(addressDto));
            person.addAddress(address);
            return personMapper.personToDto(personRepository.save(person));
        }

        return null;
    }

    public List<PersonDto> allSavedPersons() {
        List<Person> allSavedPersonDtos = personRepository.findAll();
        return allSavedPersonDtos.stream()
                .map(savedPerson -> personMapper.personToDto(savedPerson))
                .collect(Collectors.toList());
    }

    public int numberOfSavedPersons() {
        return personRepository.findAll().size();
    }
}
