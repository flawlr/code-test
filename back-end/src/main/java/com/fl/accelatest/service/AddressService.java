package com.fl.accelatest.service;

import com.fl.accelatest.model.AddressDto;
import com.fl.accelatest.entity.Address;
import com.fl.accelatest.mapper.AddressMapper;
import com.fl.accelatest.respository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AddressService {
    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private AddressMapper addressMapper;

    public AddressDto saveAddress(AddressDto addressDto) {
        Address address = addressMapper.dtoToAddress(addressDto);
        return addressMapper.addressToDto(addressRepository.save(address));
    }

    public AddressDto updateAddress(AddressDto addressDto) {
        Optional<Address> existingAddressOptional = addressRepository.findById(addressDto.getId());
        if (existingAddressOptional.isPresent()) {
            Address existingAddress = existingAddressOptional.get();
            if (addressDto.getStreet() != null) {
                existingAddress.setStreet(addressDto.getStreet());
            }
            if (addressDto.getCity() != null) {
                existingAddress.setCity(addressDto.getCity());
            }
            if (addressDto.getState() != null) {
                existingAddress.setState(addressDto.getState());
            }
            if (addressDto.getPostalCode() != null) {
                existingAddress.setPostalCode(addressDto.getPostalCode());
            }
            return addressMapper.addressToDto(addressRepository.save(existingAddress));
        }
        return null;
    }

    public AddressDto deleteAddress(Long id) {
        Optional<Address> address = addressRepository.findById(id);
        if (address.isPresent()) {
            addressRepository.deleteById(id);
            return addressMapper.addressToDto(address.get());
        }
        return null;
    }
}
