package com.fl.accelatest.mapper;

import com.fl.accelatest.model.AddressDto;
import com.fl.accelatest.entity.Address;
import org.springframework.stereotype.Component;

@Component
public class AddressMapper {
    public Address dtoToAddress(AddressDto addressDto) {
        return new Address(addressDto.getId(), addressDto.getStreet(), addressDto.getCity(), addressDto.getState(), addressDto.getPostalCode());
    }

    public AddressDto addressToDto(Address address) {
        return new AddressDto(address.getId(), address.getStreet(), address.getCity(), address.getState(), address.getPostalCode());
    }
}
