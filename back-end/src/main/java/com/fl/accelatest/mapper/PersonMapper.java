package com.fl.accelatest.mapper;

import com.fl.accelatest.model.AddressDto;
import com.fl.accelatest.model.PersonDto;
import com.fl.accelatest.entity.Address;
import com.fl.accelatest.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PersonMapper {
    @Autowired
    AddressMapper addressMapper;

    public Person dtoToPerson(PersonDto personDto) {
        List<Address> addresses = new ArrayList<>();
        if (personDto.getAddresses() != null) {
             addresses = personDto.getAddresses().stream()
                    .map(addressDto -> addressMapper.dtoToAddress(addressDto))
                    .collect(Collectors.toList());
        }

        return new Person(personDto.getId(), personDto.getFirstName(), personDto.getLastName(), addresses);
    }

    public PersonDto personToDto(Person person) {
        List<AddressDto> addressDtos = new ArrayList<>();
        if (person.getAddresses() != null) {
            addressDtos = person.getAddresses().stream()
                    .map(address -> addressMapper.addressToDto(address))
                    .collect(Collectors.toList());
        }

        return new PersonDto(person.getId(), person.getFirstName(), person.getLastName(), addressDtos);
    }
}
