package com.fl.accelatest.mapper;

import com.fl.accelatest.model.AddressDto;
import com.fl.accelatest.model.PersonDto;
import com.fl.accelatest.entity.Address;
import com.fl.accelatest.entity.Person;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonMapperTest {
    @Autowired
    PersonMapper personMapper;

    private final static Long PERSON_ID = 1L;
    private final static String FIRST_NAME = "firstName";
    private final static String LAST_NAME = "lastName";
    private final static Long ADDRESS_ID_1 = 1L;
    private final static String STREET_1 = "street1";
    private final static String CITY_1 = "city1";
    private final static String STATE_1 = "state1";
    private final static String POSTAL_CODE_1 = "postalCode1";
    private final static Long ADDRESS_ID_2 = 2L;
    private final static String STREET_2 = "street2";
    private final static String CITY_2 = "city2";
    private final static String STATE_2 = "state2";
    private final static String POSTAL_CODE_2 = "postalCode2";

    @Test
    public void shouldMapAddressDtoToAddress() {
        List<AddressDto> addressesDtos = new ArrayList<>() {{
            add(new AddressDto(ADDRESS_ID_1, STREET_1, CITY_1, STATE_1, POSTAL_CODE_1));
            add(new AddressDto(ADDRESS_ID_2, STREET_2, CITY_2, STATE_2, POSTAL_CODE_2));
        }};

        PersonDto personDto = new PersonDto(PERSON_ID, FIRST_NAME, LAST_NAME, addressesDtos);
        Person person = personMapper.dtoToPerson(personDto);

        assertNotNull(person);
        assertEquals(person.getId(), PERSON_ID);
        assertEquals(person.getFirstName(), FIRST_NAME);
        assertEquals(person.getLastName(), LAST_NAME);
        assertNotNull(person.getAddresses());
        List<Address> addresses = person.getAddresses();
        assertEquals(2, addresses.size());
        validateAddress(addresses.get(0), ADDRESS_ID_1, STREET_1, CITY_1, STATE_1, POSTAL_CODE_1);
        validateAddress(addresses.get(1), ADDRESS_ID_2, STREET_2, CITY_2, STATE_2, POSTAL_CODE_2);
    }

    private void validateAddress(Address address, Long id, String street, String city, String state, String postalCode) {
        assertNotNull(address);
        assertEquals(address.getId(), id);
        assertEquals(address.getStreet(), street);
        assertEquals(address.getCity(), city);
        assertEquals(address.getState(), state);
        assertEquals(address.getPostalCode(), postalCode);
    }

    @Test
    public void shouldMapAddressToAddressDto() {
        List<Address> addresses = new ArrayList<>() {{
            add(new Address(ADDRESS_ID_1, STREET_1, CITY_1, STATE_1, POSTAL_CODE_1));
            add(new Address(ADDRESS_ID_2, STREET_2, CITY_2, STATE_2, POSTAL_CODE_2));
        }};

        Person person = new Person(PERSON_ID, FIRST_NAME, LAST_NAME, addresses);
        PersonDto personDto = personMapper.personToDto(person);

        assertNotNull(personDto);
        assertEquals(personDto.getId(), PERSON_ID);
        assertEquals(personDto.getFirstName(), FIRST_NAME);
        assertEquals(personDto.getLastName(), LAST_NAME);

        List<AddressDto> addressDtos = personDto.getAddresses();
        assertEquals(2, addressDtos.size());
        validateAddressDto(addressDtos.get(0), ADDRESS_ID_1, STREET_1, CITY_1, STATE_1, POSTAL_CODE_1);
        validateAddressDto(addressDtos.get(1), ADDRESS_ID_2, STREET_2, CITY_2, STATE_2, POSTAL_CODE_2);
    }

    private void validateAddressDto(AddressDto addressDto, Long id, String street, String city, String state, String postalCode) {
        assertNotNull(addressDto);
        assertEquals(addressDto.getId(), id);
        assertEquals(addressDto.getStreet(), street);
        assertEquals(addressDto.getCity(), city);
        assertEquals(addressDto.getState(), state);
        assertEquals(addressDto.getPostalCode(), postalCode);
    }
}
