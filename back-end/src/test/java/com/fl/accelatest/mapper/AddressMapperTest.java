package com.fl.accelatest.mapper;

import com.fl.accelatest.model.AddressDto;
import com.fl.accelatest.entity.Address;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AddressMapperTest {
    private final static Long ADDRESS_ID = 1L;
    private final static String STREET = "street";
    private final static String CITY = "city";
    private final static String STATE = "state";
    private final static String POSTAL_CODE = "postalCode";

    @Test
    public void shouldMapAddressDtoToAddress() {
        AddressDto addressDto = new AddressDto(ADDRESS_ID, STREET, CITY, STATE, POSTAL_CODE);
        AddressMapper addressMapper = new AddressMapper();
        Address address = addressMapper.dtoToAddress(addressDto);

        assertNotNull(address);
        assertEquals(address.getId(), ADDRESS_ID);
        assertEquals(address.getStreet(), STREET);
        assertEquals(address.getCity(), CITY);
        assertEquals(address.getState(), STATE);
        assertEquals(address.getPostalCode(), POSTAL_CODE);
    }

    @Test
    public void shouldMapAddressToAddressDto() {
        Address address = new Address(ADDRESS_ID, STREET, CITY, STATE, POSTAL_CODE);
        AddressMapper addressMapper = new AddressMapper();
        AddressDto addressDto = addressMapper.addressToDto(address);

        assertNotNull(addressDto);
        assertEquals(addressDto.getId(), ADDRESS_ID);
        assertEquals(addressDto.getStreet(), STREET);
        assertEquals(addressDto.getCity(), CITY);
        assertEquals(addressDto.getState(), STATE);
        assertEquals(addressDto.getPostalCode(), POSTAL_CODE);
    }
}
