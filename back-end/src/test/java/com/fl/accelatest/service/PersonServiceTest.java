package com.fl.accelatest.service;

import com.fl.accelatest.model.AddressDto;
import com.fl.accelatest.model.PersonDto;
import com.fl.accelatest.entity.Address;
import com.fl.accelatest.entity.Person;
import com.fl.accelatest.respository.PersonRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest
public class PersonServiceTest {
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonService personService;

    @Autowired
    private AddressService addressService;

    private final static Long PERSON_ID = 1L;
    private final static String FIRST_NAME = "firstName";
    private final static String LAST_NAME = "lastName";
    private final static Long PERSON_ID_2 = 2L;
    private final static String FIRST_NAME_2 = "firstName2";
    private final static String LAST_NAME_2 = "lastName2";
    private final static String FIRST_NAME_UPDATED = "firstNameUpdated";
    private final static String LAST_NAME_UPDATED = "lastNameUpdated";
    private final static Long ADDRESS_ID = 1L;
    private final static String STREET = "street";
    private final static String CITY = "city";
    private final static String STATE = "state";
    private final static String POSTAL_CODE = "postalCode";

    @Test
    public void shouldSavePerson() {
        List<AddressDto> addressDtos = createAddressDtos();
        PersonDto personDto = new PersonDto(PERSON_ID, FIRST_NAME, LAST_NAME, addressDtos);

        List<Person> personListBeforeSaving = personRepository.findAll();
        assertEquals(0, personListBeforeSaving.size());

        personService.savePerson(personDto);

        List<Person> personListAfterSaving = personRepository.findAll();
        assertEquals(1, personListAfterSaving.size());

        Person person = personListAfterSaving.get(0);
        assertEquals(person.getId(), ADDRESS_ID);
        assertEquals(person.getFirstName(), FIRST_NAME);
        assertEquals(person.getLastName(), LAST_NAME);
        Address firstAddress = person.getAddresses().get(0);
        validateAddress(firstAddress, firstAddress.getId(), firstAddress.getStreet(), firstAddress.getCity(), firstAddress.getState(), firstAddress.getPostalCode());
    }

    private List<AddressDto> createAddressDtos() {
        AddressDto addressDto = new AddressDto(ADDRESS_ID, STREET, CITY, STATE, POSTAL_CODE);
        addressService.saveAddress(addressDto);
        return new ArrayList<>() {{
            add(addressDto);
        }};
    }

    private void validateAddress(Address address, Long id, String street, String city, String state, String postalCode) {
        assertNotNull(address);
        assertEquals(address.getId(), id);
        assertEquals(address.getStreet(), street);
        assertEquals(address.getCity(), city);
        assertEquals(address.getState(), state);
        assertEquals(address.getPostalCode(), postalCode);
    }

    @Test
    public void shouldUpdatePerson() {
        PersonDto personDto = new PersonDto(PERSON_ID, FIRST_NAME, LAST_NAME, null);
        personService.savePerson(personDto);

        Person personBeforeUpdating = personRepository.findById(PERSON_ID).get();

        assertEquals(personBeforeUpdating.getId(), PERSON_ID);
        assertEquals(personBeforeUpdating.getFirstName(), FIRST_NAME);
        assertEquals(personBeforeUpdating.getLastName(), LAST_NAME);

        PersonDto personDtoUpdated = new PersonDto(PERSON_ID, FIRST_NAME_UPDATED, LAST_NAME_UPDATED, null);
        personService.updatePerson(personDtoUpdated);

        Person personAfterUpdating = personRepository.findById(PERSON_ID).get();
        assertEquals(personAfterUpdating.getId(), PERSON_ID);
        assertEquals(personAfterUpdating.getFirstName(), FIRST_NAME_UPDATED);
        assertEquals(personAfterUpdating.getLastName(), LAST_NAME_UPDATED);
    }

    @Test
    public void shouldUpdatePerson_NullFirstName() {
        PersonDto personDto = new PersonDto(PERSON_ID, FIRST_NAME, LAST_NAME, null);
        personService.savePerson(personDto);

        Person personBeforeUpdating = personRepository.findById(PERSON_ID).get();

        assertEquals(personBeforeUpdating.getId(), PERSON_ID);
        assertEquals(personBeforeUpdating.getFirstName(), FIRST_NAME);
        assertEquals(personBeforeUpdating.getLastName(), LAST_NAME);

        PersonDto personDtoUpdated = new PersonDto(PERSON_ID, null, LAST_NAME_UPDATED, null);
        personService.updatePerson(personDtoUpdated);

        Person personAfterUpdating = personRepository.findById(PERSON_ID).get();
        assertEquals(personAfterUpdating.getId(), PERSON_ID);
        assertEquals(personAfterUpdating.getFirstName(), FIRST_NAME);
        assertEquals(personAfterUpdating.getLastName(), LAST_NAME_UPDATED);
    }

    @Test
    public void shouldDeletePerson() {
        PersonDto personDto = new PersonDto(PERSON_ID, FIRST_NAME, LAST_NAME, null);

        personService.savePerson(personDto);

        List<Person> personListAfterSaving = personRepository.findAll();
        assertEquals(1, personListAfterSaving.size());

        personService.deletePerson(PERSON_ID);

        List<Person> personListAfterDeleting = personRepository.findAll();
        assertEquals(0, personListAfterDeleting.size());
    }

    @Test
    public void shouldAddAddress() {
        PersonDto personDto = new PersonDto(PERSON_ID, FIRST_NAME, LAST_NAME, new ArrayList<>());
        personService.savePerson(personDto);

        Person savedPerson = personRepository.findById(PERSON_ID).get();
        assertEquals(0, savedPerson.getAddresses().size());

        AddressDto addressDto = new AddressDto(ADDRESS_ID, STREET, CITY, STATE, POSTAL_CODE);
        personService.addAddress(savedPerson.getId(), addressDto);

        Person savedPersonAfterAddingAddress = personRepository.findById(PERSON_ID).get();
        assertEquals(1, savedPersonAfterAddingAddress.getAddresses().size());
    }

    @Test
    public void shouldReturnListOfAllPersons() {
        PersonDto personDto = new PersonDto(PERSON_ID, FIRST_NAME, LAST_NAME, null);
        PersonDto personDto2 = new PersonDto(PERSON_ID_2, FIRST_NAME_2, LAST_NAME_2, null);

        List<PersonDto> allPersonsBeforeSaving = personService.allSavedPersons();
        assertEquals(0, allPersonsBeforeSaving.size());

        personService.savePerson(personDto);
        personService.savePerson(personDto2);

        List<PersonDto> allPersonsAfterSaving = personService.allSavedPersons();
        assertEquals(2, allPersonsAfterSaving.size());
    }

    @Test
    public void shouldReturnNumberOfSavedPersons() {
        assertEquals(0, personService.numberOfSavedPersons());

        PersonDto personDto = new PersonDto(PERSON_ID, FIRST_NAME, LAST_NAME, null);
        personService.savePerson(personDto);

        assertEquals(1, personService.numberOfSavedPersons());

        PersonDto personDto2 = new PersonDto(PERSON_ID_2, FIRST_NAME_2, LAST_NAME_2, null);
        personService.savePerson(personDto2);

        assertEquals(2, personService.numberOfSavedPersons());
    }
}
