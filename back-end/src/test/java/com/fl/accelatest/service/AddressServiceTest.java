package com.fl.accelatest.service;

import com.fl.accelatest.model.AddressDto;
import com.fl.accelatest.entity.Address;
import com.fl.accelatest.respository.AddressRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AddressServiceTest {
    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private AddressService addressService;

    private final static Long ADDRESS_ID = 1L;
    private final static String STREET = "street";
    private final static String CITY = "city";
    private final static String STATE = "state";
    private final static String POSTAL_CODE = "postalCode";
    private final static String STREET_UPDATED = "street_updated";
    private final static String CITY_UPDATED = "city_updated";
    private final static String STATE_UPDATED = "state_updated";
    private final static String POSTAL_CODE_UPDATED = "postalCode_updated";

    @Test
    public void shouldSaveAddress() {
        AddressDto addressDto = new AddressDto(ADDRESS_ID, STREET, CITY, STATE, POSTAL_CODE);

        List<Address> addressListBeforeSaving = addressRepository.findAll();
        assertEquals(0, addressListBeforeSaving.size());

        addressService.saveAddress(addressDto);

        List<Address> addressListAfterSaving = addressRepository.findAll();
        assertEquals(1, addressListAfterSaving.size());

        Address address = addressListAfterSaving.get(0);
        assertEquals(address.getId(), ADDRESS_ID);
        assertEquals(address.getStreet(), STREET);
        assertEquals(address.getCity(), CITY);
        assertEquals(address.getState(), STATE);
        assertEquals(address.getPostalCode(), POSTAL_CODE);
    }

    @Test
    public void shouldUpdateAddress() {
        AddressDto addressDto = new AddressDto(ADDRESS_ID, STREET, CITY, STATE, POSTAL_CODE);
        addressService.saveAddress(addressDto);

        Address addressBeforeUpdating = addressRepository.findById(ADDRESS_ID).get();

        assertEquals(addressBeforeUpdating.getId(), ADDRESS_ID);
        assertEquals(addressBeforeUpdating.getStreet(), STREET);
        assertEquals(addressBeforeUpdating.getCity(), CITY);
        assertEquals(addressBeforeUpdating.getState(), STATE);
        assertEquals(addressBeforeUpdating.getPostalCode(), POSTAL_CODE);

        AddressDto addressDtoUpdated = new AddressDto(ADDRESS_ID, STREET_UPDATED, CITY_UPDATED, STATE_UPDATED, POSTAL_CODE_UPDATED);
        addressService.updateAddress(addressDtoUpdated);

        Address addressAfterUpdating = addressRepository.findById(ADDRESS_ID).get();
        assertEquals(addressAfterUpdating.getId(), ADDRESS_ID);
        assertEquals(addressAfterUpdating.getStreet(), STREET_UPDATED);
        assertEquals(addressAfterUpdating.getCity(), CITY_UPDATED);
        assertEquals(addressAfterUpdating.getState(), STATE_UPDATED);
        assertEquals(addressAfterUpdating.getPostalCode(), POSTAL_CODE_UPDATED);
    }

    @Test
    public void shouldUpdateAddress_NullStreet() {
        AddressDto addressDto = new AddressDto(ADDRESS_ID, STREET, CITY, STATE, POSTAL_CODE);
        addressService.saveAddress(addressDto);

        Address addressBeforeUpdating = addressRepository.findById(ADDRESS_ID).get();

        assertEquals(addressBeforeUpdating.getId(), ADDRESS_ID);
        assertEquals(addressBeforeUpdating.getStreet(), STREET);
        assertEquals(addressBeforeUpdating.getCity(), CITY);
        assertEquals(addressBeforeUpdating.getState(), STATE);
        assertEquals(addressBeforeUpdating.getPostalCode(), POSTAL_CODE);

        AddressDto addressDtoUpdated = new AddressDto(ADDRESS_ID, null, CITY_UPDATED, STATE_UPDATED, POSTAL_CODE_UPDATED);
        addressService.updateAddress(addressDtoUpdated);

        Address addressAfterUpdating = addressRepository.findById(ADDRESS_ID).get();
        assertEquals(addressAfterUpdating.getId(), ADDRESS_ID);
        assertEquals(addressAfterUpdating.getStreet(), STREET);
        assertEquals(addressAfterUpdating.getCity(), CITY_UPDATED);
        assertEquals(addressAfterUpdating.getState(), STATE_UPDATED);
        assertEquals(addressAfterUpdating.getPostalCode(), POSTAL_CODE_UPDATED);
    }

    @Test
    public void shouldDeleteAddress() {
        AddressDto addressDto = new AddressDto(ADDRESS_ID, STREET, CITY, STATE, POSTAL_CODE);

        addressService.saveAddress(addressDto);

        List<Address> addressListAfterSaving = addressRepository.findAll();
        assertEquals(1, addressListAfterSaving.size());

        addressService.deleteAddress(ADDRESS_ID);

        List<Address> addressListAfterDeleting = addressRepository.findAll();
        assertEquals(0, addressListAfterDeleting.size());
    }
}
