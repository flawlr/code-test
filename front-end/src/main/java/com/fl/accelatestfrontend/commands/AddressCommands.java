package com.fl.accelatestfrontend.commands;

import com.fl.accelatestfrontend.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.net.URISyntaxException;

@ShellComponent
public class AddressCommands {
    @Autowired
    AddressService addressService;

    @ShellMethod("Edit an address")
    public void editAddress(
            @ShellOption() Long addressId,
            @ShellOption() String street,
            @ShellOption() String city,
            @ShellOption() String state,
            @ShellOption() String postalCode
    ) throws URISyntaxException {
        addressService.editAddress(addressId, street, city, state, postalCode);
    }

    @ShellMethod("Delete an address")
    public void deleteAddress(
            @ShellOption() Long addressId
    ) throws URISyntaxException {
        addressService.deleteAddress(addressId);
    }
}
