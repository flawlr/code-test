package com.fl.accelatestfrontend.commands;

import com.fl.accelatestfrontend.model.PersonDto;
import com.fl.accelatestfrontend.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.net.URISyntaxException;

@ShellComponent
public class PersonCommands {
    @Autowired
    PersonService personService;

    @ShellMethod("Add a person")
    public void addPerson(
            @ShellOption() Long personId,
            @ShellOption() String firstName,
            @ShellOption() String lastName
    ) throws URISyntaxException {
        personService.addAPerson(personId, firstName, lastName);
    }

    @ShellMethod("Edit a person")
    public void editPerson(
            @ShellOption() Long personId,
            @ShellOption() String firstName,
            @ShellOption() String lastName
    ) throws URISyntaxException {
        personService.editPerson(personId, firstName, lastName);
    }

    @ShellMethod("Delete a person")
    public void deletePerson(
            @ShellOption() Long personId
    ) throws URISyntaxException {
        personService.deletePerson(personId);
    }

    @ShellMethod("Add an address to a person")
    public void addAddressToPerson(
            @ShellOption() Long personId,
            @ShellOption() Long addressId,
            @ShellOption() String street,
            @ShellOption() String city,
            @ShellOption() String state,
            @ShellOption() String postalCode
    ) throws URISyntaxException {
        personService.addAddressToPerson(personId, addressId, street, city, state, postalCode);
    }

    @ShellMethod("Print number of saved persons")
    public void numberOfPersons() throws URISyntaxException {
        System.out.println("Number of persons: " + personService.numberOfPersons());
    }

    @ShellMethod("List all saved persons")
    public void listAllPersons() throws URISyntaxException {
        PersonDto[] persons = personService.getAllPersons();

        for (PersonDto person : persons) {
            System.out.println(person.toString());
        }
    }
}
