package com.fl.accelatestfrontend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressDto {
    private Long id;
    private String street;
    private String city;
    private String state;
    private String postalCode;

    public String toString() {
        return "id: " + id + " street: " + street + " city: " + city + " state: " +
                state + " postalCode: " + postalCode;
    }
}
