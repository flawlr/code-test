package com.fl.accelatestfrontend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonDto {
    private Long id;
    private String firstName;
    private String lastName;
    private List<AddressDto> addresses;

    public String toString() {
        String addressString = addresses.stream().map(addressDto -> "\t" + addressDto.toString() + "\n").collect(Collectors.joining());
        return "id: " + id + " firstName: " + firstName + " lastName: " + lastName + "\naddresses: \n" + addressString;
    }
}