package com.fl.accelatestfrontend.http;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URISyntaxException;

@Component
public class HttpConfig {
    @Value("${config.host}")
    private String host;

    @Value("${config.port}")
    private int port;

    public URI getApplicationURI(String path) throws URISyntaxException {
        return new URIBuilder().setScheme("http").setHost(host).setPort(port).setPath(path).build();
    }
}
