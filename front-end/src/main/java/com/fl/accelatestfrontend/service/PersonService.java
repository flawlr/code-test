package com.fl.accelatestfrontend.service;

import com.fl.accelatestfrontend.http.HttpConfig;
import com.fl.accelatestfrontend.model.AddressDto;
import com.fl.accelatestfrontend.model.PersonDto;
import com.fl.accelatestfrontend.model.PersonRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Service
public class PersonService {
    @Autowired
    RestTemplate restTemplate;

    @Autowired
    HttpConfig httpClient;

    private static final String PERSONS_PATH = "persons";
    private static final String ADDRESSES_PATH = "addresses";

    public void addAPerson(Long personId, String firstName, String lastName) throws URISyntaxException {
        PersonRequest personRequest = new PersonRequest(personId, firstName, lastName);
        URI uri = httpClient.getApplicationURI(PERSONS_PATH + "/" + personId);
        restTemplate.postForEntity(uri, personRequest, PersonDto.class);
    }

    public void editPerson(Long personId, String firstName, String lastName) throws URISyntaxException {
        PersonRequest personRequest = new PersonRequest(personId, firstName, lastName);
        URI uri = httpClient.getApplicationURI(PERSONS_PATH + "/" + personId);
        restTemplate.put(uri, personRequest);
    }

    public void deletePerson(Long personId) throws URISyntaxException {
        URI uri = httpClient.getApplicationURI(PERSONS_PATH + "/" + personId);
        restTemplate.delete(uri);
    }

    public void addAddressToPerson(Long personId, Long addressId, String street, String city,
                                   String state, String postalCode) throws URISyntaxException {
        AddressDto addressDto = new AddressDto(addressId, street, city, state, postalCode);
        URI uri = httpClient.getApplicationURI(PERSONS_PATH + "/" + personId + "/" + ADDRESSES_PATH + "/" + addressId);
        restTemplate.put(uri, addressDto);
    }

    public PersonDto[] getAllPersons() throws URISyntaxException {
        URI uri = httpClient.getApplicationURI(PERSONS_PATH);
        ResponseEntity<PersonDto[]> result = restTemplate.getForEntity(uri, PersonDto[].class);
        PersonDto[] personsResponse = result.getBody();

        if (personsResponse != null) {
            return personsResponse;
        }

        return new PersonDto[0];
    }

    public int numberOfPersons() throws URISyntaxException {
        PersonDto[] personDtos = getAllPersons();

        return personDtos.length;
    }
}
