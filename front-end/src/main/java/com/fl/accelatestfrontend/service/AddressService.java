package com.fl.accelatestfrontend.service;

import com.fl.accelatestfrontend.http.HttpConfig;
import com.fl.accelatestfrontend.model.AddressDto;
import com.fl.accelatestfrontend.model.PersonRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Service
public class AddressService {
    @Autowired
    RestTemplate restTemplate;

    @Autowired
    HttpConfig httpClient;

    private static final String ADDRESSES_PATH = "addresses";

    public void editAddress(Long addressId, String street, String city, String state, String postalCode) throws URISyntaxException {
        AddressDto addressDto = new AddressDto(addressId, street, city, state, postalCode);
        URI uri = httpClient.getApplicationURI(ADDRESSES_PATH + "/" + addressId);
        restTemplate.put(uri, addressDto);
    }

    public void deleteAddress(Long addressId) throws URISyntaxException {
        URI uri = httpClient.getApplicationURI(ADDRESSES_PATH + "/" + addressId);
        restTemplate.delete(uri);
    }
}
