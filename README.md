# Francis Lawlor

The application consists of two components, the back-end, which provides an api for interacting with a database, and a front-end which enables the user to provide input.

## Front-end


### Commands

* Commands outlined in the spec are implemented with Spring Shell
* Commands can be printed by inputting `help`:

```
Address Commands
        delete-address: Delete an address
        edit-address: Edit an address

Built-In Commands
        clear: Clear the shell screen.
        exit, quit: Exit the shell.
        help: Display help about available commands.
        script: Read and execute commands from a file.
        stacktrace: Display the full stacktrace of the last error.

Person Commands
        add-address-to-person: Add an address to a person
        add-person: Add a person
        delete-person: Delete a person
        edit-person: Edit a person
        list-all-persons: List all saved persons
        number-of-persons: Print number of saved persons
```

* Example command inputs:

`list-all-persons`
`number-of-persons`
`add-person 1 Thierry Henry`
`edit-person 1 Thierry Ronaldo`
`add-address-to-person 1 1 "fake street" "fake city" "fake state" "fake postal code"`
`edit-address 1 "real street" "real city" "real state" "real postal code"`
`delete-address 1`
`delete-person 1`

## Back-end

* Implemented with Spring
* Contains controllers for address and person
* Contains repositories for address and person
* Contains services for address and person logic
* Contains tests
* Uses lombok
* Uses h2 in-memory db

## Running the app

* To run the back-end navigate to `back-end` and run `gradle bootRun`
* To run the front-end navigate to `front-end` and run `gradle bootRun`
* You can execute the commands above in the front-end's console
* Note: You will need the lombok plugin for intellij and to enable annotation processing

## Points of Note

I didn't have much time to work on this. Things I would do differently include:

* Add persistent data storage. This would involve providing a new profile with datasource configuration for the back-end and providing a Dockerfile which would create a local instance of a dbms e.g. postgres
* Dockerize the whole application. This would involve creating a separate Dockerfile for the front-end and back-end, along with a docker compose file to start them up with the db.
* Write more comprehensive tests. I didn't test the Controllers or any of the front-end code.
* Add swagger to document the Endpoints
